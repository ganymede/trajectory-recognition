from glob import glob
import logging
import sys
import os

import cv2
import pybgs as bgs
import numpy as np
import matplotlib.pyplot as plt
from sklearn import cluster
from skimage.color import label2rgb

if __name__ == '__main__':
    # ci: Are we using CI (continuous integration)?
    ci = sys.argv[1] == 'ci' if len(sys.argv) > 1 else None
    loglevel = logging.INFO if ci else logging.INFO
    logging.basicConfig(level=loglevel,
                        format='%(name)s\t[%(levelname)-8s] %(message)s')
    logger = logging.getLogger('Trajectory Recognition')

    # Get all filenames
    filenames = glob('sample-videos/*.mp4', recursive=True)

    # Define files to process
    all_files = filenames

    bgs_algorithms = [
        bgs.FrameDifference(),
        bgs.WeightedMovingMean(),
    ]
    clustering_algorithms = [
                                ['DBSCAN-%d-%d' % (i, j), cluster.DBSCAN,
                                 dict(eps=i,
                                      min_samples=j)]
                                for i in [2, 5, 10]
                                for j in [2, 4]
                            ] 
                            # Example for other cluster algorithm:
                            #+ [
                                #['OPTICS-%d-%.2f-%d' % (i, j, k),
                                # cluster.OPTICS,
                                # dict(min_samples=i,
                                #      xi=j,
                                #      min_cluster_size=k)]
                                #for i in [1, 2, 4]
                                #for j in [.05, .1, .15]
                                #for k in [.1, .3]
                            #]

    # process files
    for file in all_files:
        logger.info('Processing file: %s', file)

        # Get path to save data
        base_dir = os.getcwd()
        base_name = file.split(os.sep)[-1][:-4]
        folder_name = os.path.join(base_dir, 'output')
        
        # Test different algorithms:
        for algorithm in bgs_algorithms:
            cap = cv2.VideoCapture(file)
            algo_name = type(algorithm).__name__
            logger.info('Processing Algorithm: %s', algo_name)

            i = 0
            added_images = {}
            background = np.array([])

            while True:
                flag, frame = cap.read()

                if flag:
                    height, width, = 500, 500
                    frame = cv2.resize(frame,
                                       (height, width),
                                       interpolation=cv2.INTER_CUBIC)

                    # Apply background subtraction
                    img_output = algorithm.apply(frame)
                    # Deleting timestamp (approx position)
                    timestamp_position = int(height*560/600), \
                                         int(width*160/600)
                    img_output[timestamp_position[0]:-1,
                               0:timestamp_position[1]] = 0

                    filename = "%s-%s" % (base_name, algo_name)
                    filepath = os.path.join(folder_name, 'img', 'Frames', filename)
                    if not os.path.exists(os.path.join(folder_name, 'img', 'Frames')):
                        os.makedirs(os.path.join(folder_name, 'img', 'Frames'))
                    
                    cv2.imwrite(filepath + '-%d-orig.jpg' % i, frame)
                    cv2.imwrite(filepath + '-%d-bg.jpg' % i, img_output)
                    
                    # Show image if needed
                    #cv2.imshow('orig', frame)
                    #cv2.imshow('background', img_output)

                    if i == 5:
                        for name, c_algo, options in clustering_algorithms:
                            added_images[name] = np.ones(img_output.shape)*(-2)
                        background = frame
                    elif i > 5:
                        img_vector = np.argwhere(img_output > 0)

                        if img_vector.shape[0] > 0:
                            for name, c_algo, options in clustering_algorithms:
                                if len(img_vector) <= \
                                        options.get('min_samples'):
                                    continue
                                find_cluster = c_algo(**options)
                                find_cluster.fit(img_vector)
                                y_pred = find_cluster.labels_.astype(np.int)
                                for cat in np.unique(y_pred):
                                    idx = img_vector[y_pred == cat]
                                    added_images[name][idx[:, 0],
                                                       idx[:, 1]] = cat
                                if i % 50 == 0:
                                    if name == 'DBSCAN-2-2':
                                        tmp_image = np.zeros_like(img_output)
                                        for cat in np.unique(y_pred):
                                            idx = img_vector[y_pred == cat]
                                            tmp_image[idx[:, 0],
                                                     idx[:, 1]] = cat
                                        tmp = label2rgb(tmp_image)
                                        plt.imsave(filepath + '-cluster.jpg', tmp)
                                    logger.debug('Frame: %s, '
                                                'Algo: %s, '
                                                'Cluster: %s' %
                                                (i, name,
                                                 len(np.unique(y_pred))))
                    i += 1
                else:
                    cv2.waitKey(1000)
                    break

                if 0xFF & cv2.waitKey(10) == 27:
                    break


            # Create directory to save data
            if not os.path.exists(folder_name):
                os.makedirs(os.path.join(base_dir, 'output', 'img'))
                # os.makedirs(os.path.join(base_dir, 'output', 'numpy'))

            # Colorize output
            for name, img in added_images.items():
                final_output = label2rgb(img, background,
                                         alpha=.5,
                                         bg_label=-2)
                logger.info('Saving Image: %s-%s '
                            '(Total-Cluster: %s)' % (base_name, name,
                                                     np.max(img)))

                # Save data
                filename = "%s-%s-%s" % (base_name, algo_name, name)
                filepath = os.path.join(folder_name, 'img', filename + '.jpg')
                plt.imsave(filepath, final_output)
                
                # For debugging save numpy array (careful: takes a lot of space):
                # filepath = os.path.join(folder_name, 'numpy', filename + '.npy')
                # np.save(filepath, added_images)

            # clean up
            cap.release()
            cv2.destroyAllWindows()
