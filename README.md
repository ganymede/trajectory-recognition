# Trajectory Recognition
This software allows to read out videos and recognize the movement
of an animal (e.g. birds or insects).
From this movement patterns the trajectory will be extrapolated.

This project is using python for analysis.

## Reading Videos
To read the videos into python the package `opencv` is used.
It is free open source software available at https://opencv.org/.

## Subtracting background
For the subtraction of the static background the free open source module
`bgslibrary` from [Andrews Sobral](https://github.com/andrewssobral/bgslibrary) 
is used.

## Installation
To use this package, you need a working python environment with the required
packages listed in `requirements.txt`.
The easiest way to set this up is using [Anaconda](https://anaconda.org):
```
conda create -n myenv --file requirements.txt 
```

### Alternative: Using Docker Image
Alternatively a docker image has already all the required packages installed and can be found on docker hub: [`ody55eus/trajectory-recognition:latest`](https://hub.docker.com/r/ody55eus/trajectory-recognition)

To run the script execute the following commands (replace `URL` to download your video):
```
docker run -it ody55eus/trajectory-recognition:latest bash
git clone https://gitlab.com/ganymede/trajectory-recognition.git 
cd trajectory-recognition 
mkdir sample-videos
wget -O sample-videos/vid1.mp4 URL
python3 find-image-clusters.py 
```

see also [.gitlab-ci.yml](.gitlab-ci.yml) for execution info and for sample videos.

# License
The code is released under the [MIT license](https://opensource.org/licenses/MIT).
