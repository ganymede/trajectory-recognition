from glob import glob
import logging
import sys
import os

import cv2
import pybgs as bgs
import numpy as np
import matplotlib.pyplot as plt

from skimage.morphology import convex_hull_image
from skimage.measure import label
from skimage.color import label2rgb

if __name__ == '__main__':
    loglevel = logging.INFO
    logging.basicConfig(level=loglevel,
                        format='%(name)s\t[%(levelname)-8s] %(message)s')
    logger = logging.getLogger('Trajectory Recognition')

    # Get all filenames
    filenames = glob('sample-videos/*.mp4', recursive=True)

    # Define files to process
    all_files = filenames

    all_algorithms = [
                      bgs.FrameDifference(),
                      bgs.WeightedMovingMean(),
                      ]

    # process files
    for file in all_files:
        logger.info('Processing file: %s', file)

        # Test different algorithms:
        for algorithm in all_algorithms:
            cap = cv2.VideoCapture(file)
            algo_name = type(algorithm).__name__
            logger.info('Processing Algorithm: %s', algo_name)

            i = 0
            added_images = np.array([])
            background = np.array([])

            while True:
                flag, frame = cap.read()

                if flag:
                    height, width, = 250, 250
                    frame = cv2.resize(frame,
                                       (height, width),
                                       interpolation=cv2.INTER_CUBIC)

                    # Apply background subtraction
                    img_output = algorithm.apply(frame)
                    # Deleting timestamp
                    timestamp_position = int(height*560/600), \
                                         int(width*155/600)
                    img_output[timestamp_position[0]:-1,
                               0:timestamp_position[1]] = 0

                    if i == 5:
                        background = frame
                        added_images = np.ones_like(img_output)*-2
                    elif i > 5:
                        label_image = label(img_output,
                                            background=0,
                                            connectivity=2)
                        for cat in np.unique(label_image):
                            if cat == 0: continue
                            idx = np.argwhere(label_image == cat)
                            added_images[idx[:,0], idx[:,1]] = cat

                        if i % 50 == 0:
                            logger.info('Frame: %s, '
                                        'Cluster: %s' %
                                        (i, label_image.max()))
                    i += 1
                else:
                    cv2.waitKey(1000)
                    break

                if 0xFF & cv2.waitKey(10) == 27:
                    break

            # Get path to save data
            base_dir = os.getcwd()
            base_name = file.split(os.sep)[-1][:-4]
            folder_name = os.path.join(base_dir, 'output')

            # Create directory to save data
            if not os.path.exists(folder_name):
                os.makedirs(os.path.join(base_dir, 'output', 'img'))
                os.makedirs(os.path.join(base_dir, 'output', 'numpy'))

            # Colorize output
            final_output = label2rgb(added_images, background,
                                     alpha=.5, bg_label=-2)

            # Save data
            logger.info('Saving Image: %s, ' % base_name)
            filename = "%s-%s-%s" % (base_name, algo_name, 'label')
            filepath = os.path.join(folder_name, 'img', filename + '.jpg')
            plt.imsave(filepath, final_output)
            filepath = os.path.join(folder_name, 'numpy', filename + '.npy')
            np.save(filepath, added_images)

            # clean up
            cap.release()
            cv2.destroyAllWindows()
